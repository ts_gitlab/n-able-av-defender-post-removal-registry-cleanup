﻿$tempFolder = $env:temp
$registryList = $tempFolder + "\Bitdefender_Uninstaller_List.txt"
$url = "https://techsperience-static-content.s3-us-west-2.amazonaws.com/BDregistry.zip"
$zipLocation = $tempFolder + "\BDregistry.zip"

Invoke-webrequest -Uri $url -OutFile $zipLocation

Expand-Archive -LiteralPath $zipLocation -DestinationPath $tempFolder


Get-Content $registryList | ForEach-Object {

    if(Test-Path $_){

    write-host $_ "...is present"
    write-host $_ "...and its subkeys are being removed"
    Remove-Item -Path $_ -Recurse

    }

    else

    {
    write-host $_ "...is not there"

    }
}