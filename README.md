# N-Able AV Defender post-removal registry Cleanup

WARNING: Do not expect the s3 zip file in this example to be there, update the url per directions below.

WARNING: This is lightly tested and for our own use, if you choose to use this have fun, but we accept no liability for any issues it may cause!


Removes lingering registry keys after n-able security uninstall.

First uninstall using Add/remove programs or the N-Able removal tool, then run this to remove lingering registry keys.


To create the zip file:

1.  Replace the "$url" variable in the ps1 script to an http endpoint that your tool has access to (like a public s3 bucket).
2.  Zip the .ps1 and .txt file and upload to the location with the name you identified
3.  Upload script to your tool
4.  Enjoy

For older machines without Powershell 3 or greater, you can just run this from an elevated command line, for each key in the .txt file:

reg delete HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Ignis /f


